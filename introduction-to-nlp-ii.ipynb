{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "\n",
    "![deep.png](../deep.png)\n",
    "\n",
    "# NLP II: `CountVectorizer` and `TfidfVectorizer`\n",
    "\n",
    "\n",
    "\n",
    "---\n",
    "\n",
    "\n",
    "![](https://snag.gy/uvESGH.jpg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Learning Objectives\n",
    "---\n",
    "\n",
    "*After this lesson, you will be able to:*\n",
    "\n",
    "- Extract features from unstructured text with `sklearn`\n",
    "- Describe how count vectorization and TF-IDF work.\n",
    "- Implement `CountVectorizer` in a spam classification model.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Imports\n",
    "---\n",
    "\n",
    "We'll need the following libraries for today's lecture:\n",
    "- `pandas`\n",
    "- `CountVectorizer` and `TfidfVectorizer` from `sklearn.feature_extraction.text`\n",
    "- `Pipeline`\n",
    "- `train_test_split` and `GridSearchCV`\n",
    "- `LogisticRegression`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "from sklearn.linear_model import LogisticRegression\n",
    "from sklearn.model_selection import train_test_split, GridSearchCV\n",
    "from sklearn.pipeline import Pipeline\n",
    "from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "<a name=\"intro\"></a>\n",
    "## Introduction to Text Feature Extraction\n",
    "\n",
    "---\n",
    "\n",
    "The models we’ve been using so far accept a two-dimensional matrix of real numbers as input `X` and a target vector of classes or numbers as `y`. What if our features are not given in the form of a table of numbers but rather are unstructured? This is the case when we work with text documents.\n",
    "\n",
    "> We need a way to go from unstructured data to our numeric `X` matrix in order to use the same models. This is called _feature extraction_, and this lesson is dedicated to it.\n",
    "\n",
    "The applications of using text data in statistical modeling are practically infinite. Some examples include:\n",
    "\n",
    "- Sentiment analysis of Yelp reviews.\n",
    "- Identifying topics of new articles.\n",
    "- Classification of political authors.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "<a id='simple'></a>\n",
    "## A Simple Example\n",
    "---\n",
    "\n",
    "Suppose we're building a model that predicts whether a sentence is from a children's book or not. The inputs are strings and the output is a binary label.\n",
    "\n",
    "Here are some sample inputs:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "s1 = 'Spot is a dog.'\n",
    "s2 = 'Run Spot Run.'\n",
    "s3 = 'Run Forrest, Run!'\n",
    "s4 = 'The quick brown fox jumped over the lazy dog.'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Basic terminology\n",
    "\n",
    "---\n",
    "\n",
    "Virtually all NLP uses this base terminology:\n",
    "\n",
    "- a collection of text is a **document**. You can think of a document as a row in your feature matrix.\n",
    "- a collection of documents is a **corpus** (plural corpora)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Build a corpus\n",
    "train_corpus = [s1,s2,s3]\n",
    "test_corpus  = [s4]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "<a id='bow'></a>\n",
    "## Bag of Words/Word Counting\n",
    "---\n",
    "\n",
    "The bag-of-words model is a simplified representation of the raw data. In this model, text (such as a sentence or document) is represented as the bag (multiset) of its words.\n",
    "\n",
    "Bag-of-words representations discard grammar, order, and structure in the text but track occurrences."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "<a name=\"countvectorizer\"></a>\n",
    "## Demo: Scikit-Learn `CountVectorizer`\n",
    "---\n",
    "\n",
    "Scikit-learn offers a `CountVectorizer` class with many configurable options:\n",
    "\n",
    "**Note**: There are several parameters to tweak."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Instantiate a CountVectorizer\n",
    "cvec = CountVectorizer()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Fit the vectorizer on our corpus\n",
    "cvec.fit(train_corpus)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Transform the corpus\n",
    "X_train = cvec.transform(train_corpus)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[1, 0, 1, 0, 1],\n",
       "       [0, 0, 0, 2, 1],\n",
       "       [0, 1, 0, 2, 0]])"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Convert X_train into a DataFrame\n",
    "X_train.toarray()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>dog</th>\n",
       "      <th>forrest</th>\n",
       "      <th>is</th>\n",
       "      <th>run</th>\n",
       "      <th>spot</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>1</td>\n",
       "      <td>0</td>\n",
       "      <td>1</td>\n",
       "      <td>0</td>\n",
       "      <td>1</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>2</td>\n",
       "      <td>1</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>0</td>\n",
       "      <td>1</td>\n",
       "      <td>0</td>\n",
       "      <td>2</td>\n",
       "      <td>0</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "   dog  forrest  is  run  spot\n",
       "0    1        0   1    0     1\n",
       "1    0        0   0    2     1\n",
       "2    0        1   0    2     0"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "pd.DataFrame(X_train.toarray(), columns=cvec.get_feature_names())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['dog', 'forrest', 'is', 'run', 'spot']"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "cvec.get_feature_names()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Transform test\n",
    "X_test = cvec.transform(test_corpus)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>dog</th>\n",
       "      <th>forrest</th>\n",
       "      <th>is</th>\n",
       "      <th>run</th>\n",
       "      <th>spot</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>1</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "   dog  forrest  is  run  spot\n",
       "0    1        0   0    0     0"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "pd.DataFrame(X_test.toarray(),columns=cvec.get_feature_names())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id='stopwords'></a>\n",
    "## Stop Words\n",
    "\n",
    "---\n",
    "\n",
    "Some words are commonly used and provide no legitimate information about the content of the text. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "frozenset({'fire', 'these', 'found', 'further', 'must', 'serious', 'thereupon', 'eleven', 'once', 'whereby', 'whereas', 'amount', 'those', 'becomes', 'up', 'con', 'both', 'do', 'in', 'yet', 'ours', 'cannot', 'too', 'else', 'nobody', 'well', 'everywhere', 'whereafter', 'yours', 'beforehand', 'which', 'however', 'much', 'upon', 'being', 'seeming', 'yourselves', 'due', 'de', 'any', 'it', 'or', 'themselves', 'ever', 'for', 'sincere', 'third', 'fifteen', 'some', 'un', 'anything', 'us', 'her', 'becoming', 'fill', 'hundred', 'made', 'whence', 'about', 'throughout', 'whither', 'few', 'within', 'every', 'no', 'thus', 'rather', 'than', 'move', 'so', 'such', 'nowhere', 'own', 'above', 'done', 'had', 'namely', 'together', 'could', 'whatever', 'get', 'is', 'interest', 'afterwards', 'somewhere', 'thick', 'again', 'although', 'whether', 'among', 'against', 'around', 'our', 'but', 'hers', 'next', 'none', 'be', 'sometime', 'this', 'seem', 'have', 'many', 'may', 'eg', 'eight', 'under', 'therefore', 'anyway', 'whereupon', 'toward', 'formerly', 'wherein', 'per', 'detail', 'name', 'thereby', 'last', 'two', 'somehow', 'someone', 'find', 'ourselves', 'side', 'him', 'everything', 'three', 'cant', 'between', 'down', 'less', 'whose', 'another', 'can', 'beyond', 'four', 'keep', 'me', 'now', 'other', 'seemed', 'therein', 'with', 'if', 'its', 'on', 'the', 'before', 'moreover', 'their', 'below', 'hereafter', 'part', 'though', 'whoever', 'nevertheless', 'except', 'ten', 'former', 'same', 'very', 'whole', 'go', 'there', 'at', 'never', 'all', 'give', 'onto', 'see', 'noone', 'take', 'bill', 'towards', 'whom', 'via', 'as', 'nor', 'when', 'became', 'thence', 'off', 'been', 'and', 'elsewhere', 'anyone', 'call', 'that', 'of', 'twenty', 'where', 'either', 'first', 'herein', 'nothing', 'sixty', 'system', 'five', 'how', 'one', 'nine', 'enough', 'still', 'should', 'top', 'yourself', 'an', 'indeed', 'then', 'was', 'were', 'has', 'empty', 'anyhow', 'latterly', 'my', 'will', 'your', 'might', 'ie', 'mine', 'across', 'put', 'through', 'because', 'along', 'thin', 'please', 'become', 'here', 'otherwise', 'seems', 'behind', 'since', 'them', 'his', 'etc', 'ltd', 'perhaps', 'to', 'sometimes', 'bottom', 'several', 'something', 'why', 'hereupon', 'even', 'during', 'without', 'are', 'thereafter', 'amongst', 'whenever', 'mostly', 'would', 'we', 'only', 'thru', 'everyone', 'hence', 'myself', 'neither', 'itself', 'anywhere', 'also', 'front', 'herself', 'while', 'fifty', 'full', 'co', 'he', 'wherever', 'couldnt', 'after', 'they', 'hasnt', 'back', 'inc', 'most', 'beside', 'over', 'always', 'often', 'until', 'amoungst', 'forty', 'out', 'already', 'show', 'by', 'a', 'mill', 'meanwhile', 'almost', 'from', 'hereby', 'six', 'twelve', 'besides', 'into', 'others', 'describe', 'himself', 'latter', 'least', 'more', 'alone', 'she', 'you', 'not', 'what', 're', 'who', 'am', 'i', 'each', 'cry'})\n"
     ]
    }
   ],
   "source": [
    "from sklearn.feature_extraction import stop_words\n",
    " \n",
    "print(stop_words.ENGLISH_STOP_WORDS)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`CountVectorizer` gives you the option to eliminate stop words from your corpus.\n",
    "\n",
    "```python\n",
    "cv = CountVectorizer(stop_words='english')\n",
    "```\n",
    "\n",
    "You can optionally pass your own list of stop words that you'd like to remove.\n",
    "```python\n",
    "cv = CountVectorizer(stop_words=['list', 'of', 'words', 'to', 'stop'])\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Vocabulary size\n",
    "\n",
    "---\n",
    "One downside to `CountVectorizer` is the size of its vocabulary (`cv.get_feature_names()`) can get rather large. To mitigate this problem, you can set `max_features` to only include the N most popular vocabulary words in the corpus.\n",
    "\n",
    "```python\n",
    "cv = CountVectorizer(max_features=1000) # Only the top 1,000 words from the entire corpus will be saved\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## N-Gram Range\n",
    "---\n",
    "\n",
    "`CountVectorizer` has the ability to capture n-word phrases, also called n-grams. Consider the following:\n",
    "\n",
    "> The quick brown fox jumped over the lazy dog.\n",
    "\n",
    "In the example sentence, the 2-grams (aka bi-grams) are:\n",
    "- 'the quick'\n",
    "- 'quick brown'\n",
    "- 'brown fox'\n",
    "- 'fox jumped'\n",
    "- 'jumped over'\n",
    "- 'over the'\n",
    "- 'the lazy'\n",
    "- 'lazy dog'\n",
    "\n",
    "And the 3-grams are:\n",
    "- 'the quick brown'\n",
    "- 'quick brown fox'\n",
    "- 'brown fox jumped'\n",
    "- 'fox jumped over'\n",
    "- 'jumped over the'\n",
    "- 'over the lazy'\n",
    "- 'the lazy dog'\n",
    "\n",
    "The `ngram_range` determines what n-grams should be considered as features.\n",
    "\n",
    "```python\n",
    "cv = CountVectorizer(ngram_range=(1,2)) # Captures every single word and every 2-gram\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Min/Max Document Frequency\n",
    "---\n",
    "\n",
    "We can tell `CountVectorizer` to only consider words that occur within a certain threshold in the corpus.\n",
    "\n",
    "For example, if we only want `CountVectorizer` to add words that occur in **at least** two documents, we can do the following:\n",
    "\n",
    "```python\n",
    "cv = CountVectorizer(min_def=2) # A word must occur in at least two documents from the corpus\n",
    "```\n",
    "\n",
    "Conversely, we can set an upper threshold with `max_df`:\n",
    "\n",
    "```python\n",
    "cv = CountVectorizer(max_def=.98) # Ignore words that occur in > 98% of the documents from the corpus\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Spam classification model\n",
    "---\n",
    "Let's test this stuff out on some SMS text data.  Can you predict real vs. promotional texts just based on what is written?  Let's see...\n",
    "\n",
    "> This data set was taken from the [UCI Machine Learning Repository](https://archive.ics.uci.edu/ml/datasets/sms+spam+collection)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load the data from datasets/SMSSpamCollection\n",
    "df = pd.read_csv('datasets/SMSSpamCollection', sep='\\t', names=['label', 'message'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Data Cleaning\n",
    "---\n",
    "\n",
    "Convert ham/spam into binary labels:\n",
    "- 0 for ham\n",
    "- 1 for spam"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>label</th>\n",
       "      <th>message</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>0</td>\n",
       "      <td>Go until jurong point, crazy.. Available only ...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>0</td>\n",
       "      <td>Ok lar... Joking wif u oni...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>1</td>\n",
       "      <td>Free entry in 2 a wkly comp to win FA Cup fina...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>3</th>\n",
       "      <td>0</td>\n",
       "      <td>U dun say so early hor... U c already then say...</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>4</th>\n",
       "      <td>0</td>\n",
       "      <td>Nah I don't think he goes to usf, he lives aro...</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "   label                                            message\n",
       "0      0  Go until jurong point, crazy.. Available only ...\n",
       "1      0                      Ok lar... Joking wif u oni...\n",
       "2      1  Free entry in 2 a wkly comp to win FA Cup fina...\n",
       "3      0  U dun say so early hor... U c already then say...\n",
       "4      0  Nah I don't think he goes to usf, he lives aro..."
      ]
     },
     "execution_count": 27,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Binarize label column\n",
    "df['label'] = df['label'].map({'ham':0, 'spam':1})\n",
    "df.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Baseline accuracy\n",
    "---\n",
    "\n",
    "We need to calculate baseline accuracy in order to tell if our model is outperforming the null model (predicting the majority class)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0    0.865937\n",
       "1    0.134063\n",
       "Name: label, dtype: float64"
      ]
     },
     "execution_count": 28,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Baseline accuracy\n",
    "df['label'].value_counts(normalize=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Model prep\n",
    "---\n",
    "\n",
    "Let's set up our data for modeling:\n",
    "- `X` will be the `message` column. **NOTE**: `CountVectorizer` requires a vector, so make sure you set `X` to be a `pandas` Series, **not** a DataFrame.\n",
    "- `y` will be the `label` column"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['Spot is a dog.', 'Run Spot Run.', 'Run Forrest, Run!']"
      ]
     },
     "execution_count": 30,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "train_corpus"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "metadata": {},
   "outputs": [],
   "source": [
    "X = df['message']\n",
    "y = df['label']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Train/Test split\n",
    "---\n",
    "\n",
    "Use the train_test_split function to split your data into a training set and a holdout set."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 32,
   "metadata": {},
   "outputs": [],
   "source": [
    "X_train, X_test, y_train, y_test = train_test_split(X,y,\n",
    "                                                    random_state=42,\n",
    "                                                    stratify=y)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 33,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.13406317300789664"
      ]
     },
     "execution_count": 33,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "y.mean()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 34,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.13400335008375208"
      ]
     },
     "execution_count": 34,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "y_train.mean()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 35,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.1342426417803302"
      ]
     },
     "execution_count": 35,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "y_test.mean()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Pipeline\n",
    "---\n",
    "\n",
    "Our pipeline will consist to two stages:\n",
    "1. An instance of `CountVectorizer`\n",
    "2. A `LogisticRegression` instance"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 36,
   "metadata": {},
   "outputs": [],
   "source": [
    "pipe = Pipeline([\n",
    "    ('cvec', CountVectorizer()),\n",
    "    ('lr', LogisticRegression())\n",
    "])#this is ordered by the sequence of steps laid out here "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## `GridSearchCV`\n",
    "---\n",
    "\n",
    "At this point, you could use your `pipeline` object as a model:\n",
    "\n",
    "```python\n",
    "# Evaluate how your model will perform on unseen data\n",
    "cross_val_score(pipe, X_train, y_train, cv=3).mean() \n",
    "\n",
    "# Fit your model\n",
    "pipe.fit(X_train, y_train)\n",
    "\n",
    "# Training score\n",
    "pipe.score(X_train, y_train)\n",
    "\n",
    "# Test score\n",
    "pipe.score(X_test, y_test)\n",
    "```\n",
    "\n",
    "Since we want to tune over the `CountVectorizer`, so we'll load our `pipeline` object into `GridSearchCV`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": 47,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "/anaconda3/lib/python3.6/site-packages/sklearn/linear_model/logistic.py:433: FutureWarning: Default solver will be changed to 'lbfgs' in 0.22. Specify a solver to silence this warning.\n",
      "  FutureWarning)\n"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0.9818138310600623\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "{'cvec__max_features': 1900,\n",
       " 'cvec__ngram_range': (1, 2),\n",
       " 'cvec__stop_words': None}"
      ]
     },
     "execution_count": 47,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Tune GridSearchCV\n",
    "pipe_params = {\n",
    "    'cvec__stop_words': [None, 'english'],\n",
    "    'cvec__max_features': [1250, 1500, 1900],\n",
    "    'cvec__ngram_range': [(1,1), (1,2)]\n",
    "}\n",
    "gs = GridSearchCV(pipe, param_grid=pipe_params, cv=3)\n",
    "gs.fit(X_train, y_train);\n",
    "print(gs.best_score_)\n",
    "gs.best_params_"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 39,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.9784637473079684"
      ]
     },
     "execution_count": 39,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "gs.best_score_"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 40,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.998085666427375"
      ]
     },
     "execution_count": 40,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Train score\n",
    "gs.score(X_train, y_train)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 41,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.9806173725771715"
      ]
     },
     "execution_count": 41,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Test score\n",
    "gs.score(X_test, y_test)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "<a name=\"tfidf\"></a>\n",
    "## Term Frequency-Inverse Document Frequency (TF-IDF)\n",
    "\n",
    "---\n",
    "\n",
    "A TF-IDF score tells us which words are most discriminating between documents. Words that occur often in one document but don't occur in many documents contain a great deal of discriminating power.\n",
    "\n",
    "- This weight is a statistical measure used to evaluate how important a word is to a document in a collection (corpus).\n",
    "- The importance increases in proportion to the number of times a word appears in a document but is offset by the frequency of the word in the corpus.\n",
    "\n",
    "Variations of the TF-IDF weighting scheme are often used by search engines as a central tool in scoring and ranking a document's relevance given a user query.\n",
    "\n",
    "The inverse document frequency is a measure of how much information the word provides — that is, whether the term is common or rare across all documents. It's the logarithmically scaled inverse fraction of the documents that contain the word, obtained by dividing the total number of documents by the number of documents containing the term and then taking the logarithm of that quotient.\n",
    "\n",
    "**Let's see how it's calculated:**\n",
    "\n",
    "Term frequency (`tf`) is the frequency of a certain term in a document:\n",
    "\n",
    "$$\n",
    "\\mathrm{tf}(t,d) = \\frac{N_\\text{term}}{N_\\text{terms in Document}}\n",
    "$$\n",
    "\n",
    "where\n",
    "\n",
    "- $N_\\text{term}$ is the number of times a term/word $t$ appears in document $d$\n",
    "- $N_\\text{terms in Document}$ is the number of terms/words in document $d$\n",
    "\n",
    "Inverse document frequency (`idf`) is defined as the frequency of documents that contain that term over the whole corpus:\n",
    "\n",
    "$$\n",
    "\\mathrm{idf}(t, D) = \\log\\frac{N_\\text{Documents}}{N_\\text{Documents that contain term}}\n",
    "$$\n",
    "\n",
    "where\n",
    "\n",
    "- $N_\\text{Documents}$ is the number of documents in the corpus $D$\n",
    "- $N_\\text{Documents that contain term}$ is the number of documents in $D$ that contain term/word $t$\n",
    "\n",
    "TF-IDF is then calculated as:\n",
    "\n",
    "$$\n",
    "\\mathrm{tfidf}(t,d,D) = \\mathrm{tf}(t,d) \\cdot \\mathrm{idf}(t, D)\n",
    "$$\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "> **You might ask: But what is `log` used for?**<br>\n",
    "> Good question! This is a sublinear transformation that helps separate our extremes between rare and common values.\n",
    "\n",
    "> \"...any linear function, ${\\displaystyle g}$, for sufficiently large input ${\\displaystyle f}$, grows slower than ${\\displaystyle g}$\" — Wikipedia"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "<a id='tfidf-vec'></a>\n",
    "## Practice Using the `TfidfVectorizer`\n",
    "\n",
    "---\n",
    "\n",
    "### Why Use TF-IDF?\n",
    "- Common words are penalized.\n",
    "- Rare words have more influence.\n",
    "\n",
    "Scikit-learn provides a TF-IDF vectorizer that works similarly to the other vectorizers we've covered. Notice that we can also eliminate stop words to improve our analysis.\n",
    "\n",
    "As you did above, import and initialize the `TfidfVectorizer`, then fit the spam and ham data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 48,
   "metadata": {},
   "outputs": [],
   "source": [
    "texts = [\"am a cat\", \"am a rat\", \"am a bat\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 49,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "# Fit the transformer\n",
    "tvec = TfidfVectorizer()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 51,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>am</th>\n",
       "      <th>bat</th>\n",
       "      <th>cat</th>\n",
       "      <th>rat</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>0.508542</td>\n",
       "      <td>0.000000</td>\n",
       "      <td>0.861037</td>\n",
       "      <td>0.000000</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>0.508542</td>\n",
       "      <td>0.000000</td>\n",
       "      <td>0.000000</td>\n",
       "      <td>0.861037</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>0.508542</td>\n",
       "      <td>0.861037</td>\n",
       "      <td>0.000000</td>\n",
       "      <td>0.000000</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "         am       bat       cat       rat\n",
       "0  0.508542  0.000000  0.861037  0.000000\n",
       "1  0.508542  0.000000  0.000000  0.861037\n",
       "2  0.508542  0.861037  0.000000  0.000000"
      ]
     },
     "execution_count": 51,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "df  = pd.DataFrame(tvec.fit_transform(texts).toarray(),\n",
    "                   columns=tvec.get_feature_names())\n",
    "df.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Custom Tokenizer\n",
    "\n",
    "token_func = CountVectorizer().build_tokenizer()\n",
    "\n",
    "def custom_token_func(the_document):\n",
    "    tokens = token_func(the_document)\n",
    "    # Do stemming/lemmatization\n",
    "    return [t for t in tokens]\n",
    "\n",
    "token_func('the quick brown fox')\n",
    "\n",
    "cvec = CountVectorizer(tokenizer=custom_token_func)\n",
    "\n",
    "cvec.fit_transform(train_corpus)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "<a id='resources'></a>\n",
    "## Additional Resources\n",
    "\n",
    "---\n",
    "\n",
    "- Check out this [Yelp blog post](http://engineeringblog.yelp.com/2015/09/automatically-categorizing-yelp-businesses.html) on how it completed a classification task (with more than 1,000 response variables) using restaurant review text.\n",
    "- Always check documentation: \n",
    "    - [CountVectorizer](http://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.CountVectorizer.html). \n",
    "    - [HashingVectorizer](http://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.HashingVectorizer.html). \n",
    "    - [TF-IDF](http://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.TfidfVectorizer.html).\n",
    "- A list of all stop words is available [here](https://github.com/ga-students/DSI-DC-2/blob/master/curriculum/Week-05/5.04-nlp/stop-words.txt).\n",
    "- Wikipedia's [feature hashing](https://github.com/generalassembly-studio/DSI-course-materials/tree/master/curriculum/04-lessons/week-06/4.1-lesson) and [hash functions](https://en.wikipedia.org/wiki/Hash_function) entries are a great place to turn for more information on hashing.\n",
    "- Check out Charlie Greenbacker's [introduction to NLP](http://spark-public.s3.amazonaws.com/nlp/slides/intro.pdf), which he delivered at the [DC-NLP Meetup](http://www.meetup.com/DC-NLP/).\n",
    "- Wikipedia also has a [walk through](https://en.wikipedia.org/wiki/Tf%E2%80%93idf) of TF-IDF.\n",
    "- We played with Google's [ngram tool](https://books.google.com/ngrams/graph?content=data+science&year_start=1800&year_end=2000&corpus=15&smoothing=3&share=&direct_url=t1%3B%2Cdata%20science%3B%2Cc0).\n",
    "- A hilarious data scientist has gone rogue and used NLP and eigenfaces (eigenvalues for face recognition) [for Tinder](http://dataconomy.com/hacking-tinder-with-facial-recognition-nlp/).\n",
    "- We referenced KPCB's 2016 internet trends. If you're into startups, check out [this insightful deck](http://www.kpcb.com/internet-trends).\n",
    "- [Count vectorizer documentation](http://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.CountVectorizer.html).\n",
    "- [Choosing a stemmer](https://www.elastic.co/guide/en/elasticsearch/guide/current/choosing-a-stemmer.html).\n",
    "- [Feature hashing](https://en.wikipedia.org/wiki/Feature_hashing).\n",
    "- [Term frequency-inverse document frequency](https://en.wikipedia.org/wiki/Tf%E2%80%93idf).\n",
    "- [TF-IDF vectorizer](http://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.TfidfVectorizer.html)."
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.8"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
